
<?php

Route::get('/', 'GreetingsController@index');

Route::get('/products', 'ProductsController@show');

Route::get('/products/create', 'ProductsController@create');

Route::post('/products/store', 'ProductsController@store');

Route::get('/products/{product}', 'ProductsController@show_one');

Route::get('/products/{product}/edit', 'ProductsController@edit');

Route::put('/products/{product}', 'ProductsController@update');

Route::get('/products/{product}/delete', 'ProductsController@delete');

Route::delete('/products/{product}', 'ProductsController@destroy');




Route::get('/pages', 'PagesController@show');

Route::get('/pages/create', 'PagesController@create');

Route::post('/pages/store', 'PagesController@store');

Route::get('/pages/{page}', 'PagesController@show_one');

Route::get('/pages/{page}/edit', 'PagesController@edit');

Route::put('/pages/{page}', 'PagesController@update');

Route::get('/pages/{page}/delete', 'PagesController@delete');

Route::delete('/pages/{page}', 'PagesController@destroy');



Route::get('/orders', 'OrdersController@show');

Route::get('/orders/create', 'OrdersController@create');

Route::post('/orders/store', 'OrdersController@store');

Route::get('/orders/{order}', 'OrdersController@show_one');

Route::get('/orders/{order}/delete', 'OrdersController@delete');

Route::delete('/orders/{order}', 'OrdersController@destroy');