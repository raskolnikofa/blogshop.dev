<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            [
                'customer_name' => 'Anna',
                'email' => 'anna@maria@com',
                'phone' => '+38 (066) 88 55 123',
                'feedback' => 'Thank u so much!',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
       	 	[
                'customer_name' => 'Nick',
                'email' => 'nick@rojer@com',
                'phone' => '+38 (093) 84 00 546',
                'feedback' => 'Guys, thank you for this job.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'customer_name' => 'Sonya',
                'email' => 'sonya@sunny@com',
                'phone' => '+38 (066) 29 07 452',
                'feedback' => 'Please, call me.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);

          DB::table('pages')->insert([
            [
                'title' => 'How to win a Booker prize: be under 50, enter your seventh book – about a man',
                'alias' => 'how_to_win_a_booker_prize',
                'intro' => 'If you’re sceptical about what the odds from the bookmakers or sales figures from bookshops can tell us about who’ll win the Man Booker prize in a few hours – and you should be – then perhaps you might prefer this solid number-crunching from Kiera O’Brien, charts editor at the Bookseller.',
                'content' => 'Looking at the previous 47 winners of the award, O’Brien has found that (rather unsurprisingly) the average Booker prizewinner is an English, white, privately educated man in his late 40s, who has written a book of less than 400 pages that has a male protagonist.

				The prize, she writes, has had 31 male winners, and 16 female. Ten of those have been from BAME (black, Asian and minority ethnic) backgrounds – two of them, Marlon James and Paul Beatty, in the last two years. Winners are “overwhelmingly English”, with a 26:19 ratio of private to state-educated. Nine went to Oxford, four went to Cambridge, and four didn’t go to university at all.

				The average age for a Booker winner is 48.9, while authors usually win with their seventh book. The average number of pages of the winning title is 378.

				“Books set in the past are catnip to Booker judges. In the last 10 years, four winners have been set mainly before 1950,” writes O’Brien, and “not that the Booker prize is dogged by postcolonial guilt or anything, but books set in either India or Ireland have a great chance of sweeping the board.”',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],

                [
                'title' => 'Sean Hughes tributes – and his funniest jokes',
                'alias' => 'sean_hughes_tributes',
                'intro' => 'By his mid-20s he had scooped the Perrier award and landed his own TV show. But Sean Hughes never wanted to be a stadium standup. Mark Steel and Rhona Cameron pay tribute to a troubled talent – and we pick some of his best gags',
                'content' => 'I knew Sean from before he won the Perrier award in 1990. He was a Crystal Palace fan and we used to go to the football together. I remember going to a match with him in the early 90s, when he was on the telly quite a lot, and I took him down the pub with me. He was really warm with people and we ended up staying the evening. There was a genuine charm to him that was way beyond showbiz. He liked that world: being sat in the corner of a pub with a load of people who’d been at the football. Being funny with them but not in a show-off way.',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);

          DB::table('products')->insert([
            [
                'title' => 'Warhol By Klaus Honnef',
                'alias' => 'warhol_by_klaus_honnef',
                'price' => '8.99',
                'description' => 'Style No. 5620404650302 ; Color Code: 000
Features a biography + a collection of over 100 of his greatest works. Paperback; 2015, TASCHEN. Size: 96 pages, 21.3cm x 1.3cm x 26.2 cm',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Vogue: The Covers By Dodie Kazanjian',
                'alias' => 'vogue_the_covers',
                'price' => '45',
                'description' => 'Style No. 0620415770230 ; Color Code: 000
Stunning book filled with the essential covers from throughout Vogue’s 125-year history. Complete with four removable + frameable prints. Size: 304 pages, 23.5cm x 3.2cm x 31.1cm',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ],
            [
                'title' => 'Lonely Planet’s Atlas of Adventure',
                'alias' => 'lonely_planets_atlas',
                'price' => '24.99',
                'description' => 'Style No. 0620049050257 ; Color Code: 000
Adventure-filled book from Lonely Planet with the best outdoor experiences by country. Complete with colourful + inspirational images and expert advice from Lonely Planet’s travel gurus. Size:  336 pages, 23.5cm x 27.6cm',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]
        ]);
    }
}
