@extends('template')


@section('content')
<div class="container">
<div class="row">
        <p><h3><a class="col-md-4" href="products/create"> Create new product </a></h3></p>
</div>
<div class="row">
        @foreach($products as $product)
            <div class="col-md-4">
                <h2> {{ $product->title }} </h2>
                <p>
                    {{ $product->price }}
                </p>
                <a href="/products/{{$product->alias}}" class="btn btn-default">Show more</a>
                <a href="/products/{{$product->alias}}/edit" class="btn btn-warning">Edit</a>
                <a href="/products/{{$product->alias}}/delete" class="btn btn-danger">Delete</a>

            </div>
        @endforeach


    </div>
</div>
@endsection