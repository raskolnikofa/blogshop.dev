@extends('template')


@section('content')
<div class="container">
<div class="row">
        <p><h3><a class="col-md-4" href="pages/create"> Create new article </a></h3></p>
</div>
<div class="row">
        @foreach($pages as $page)
            <div class="col-md-4">
                <h2> {{ $page->title }} </h2>
                <p>
                    {{ $page->intro }}
                </p>
                <a href="/pages/{{$page->alias}}" class="btn btn-default">Show more</a>
                <a href="/pages/{{$page->alias}}/edit" class="btn btn-warning">Edit</a>
                <a href="/pages/{{$page->alias}}/delete" class="btn btn-danger">Delete</a>

            </div>
        @endforeach


    </div>
</div>
@endsection