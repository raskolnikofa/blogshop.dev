@extends('template')

@section('content')
    <div class="container">
        <div class="row">
        	<div class="col-md-8 col-offset-2">
                <h1>{{ $order->customer_name }}</h1>
                <p>{{ $order->phone }}</p>
                <p>{{ $order->email }}</p>
                <p>{{ $order->feedback }}</p>
            </div>
        </div>
    </div>
@endsection