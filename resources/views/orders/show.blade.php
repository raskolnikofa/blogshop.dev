@extends('template')


@section('content')
<div class="container">
<div class="row">
        <p><h3><a class="col-md-4" href="orders/create"> Create new order </a></h3></p>
</div>
<div class="row">
        @foreach($orders as $order)
            <div class="col-md-4">
                <h2> {{ $order->customer_name }} </h2>
                 <p>
                    {{ $order->feedback }}
                </p>
                <a href="/orders/{{$order->id}}" class="btn btn-default">Show more</a>
                <a href="/orders/{{$order->id}}/delete" class="btn btn-danger">Delete</a>

            </div>
        @endforeach


    </div>
</div>
@endsection