@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2>Add new order:</h2>
                <form method="POST" action="/orders/store">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="customer_name">Name</label>
                        <input type="text" name="customer_name" id="customer_name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" name="phone" id="phone" class="form-control">
                    </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control">
                    </div>
                     <div class="form-group">
                        <label for="feedback">Feedback</label>
                        <textarea name="feedback" id="feedback" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary"> Save </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection