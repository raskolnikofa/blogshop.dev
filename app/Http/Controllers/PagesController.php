<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;;


class PagesController extends Controller
{
    public function show(){

        $data['pages'] = Page::all();
        return view('pages/show', $data);

    }

    public function show_one(Page $page){

        return view('pages/show_one', compact('page'));
    }

    public function create(){
        return view('pages/create');
    }

    public function store(){

        $this->validate(request(),[
            'title' => 'required|string',
            'alias' => 'required',
            'intro' => 'required',
            'content' => 'required'
        ]);

        Page::create(request()->all());
        return redirect('/pages');

    }

    public function edit(Page $page){

        return view('pages/edit', compact('page'));

    }

    public function update(Page $page){
        $page->update(request()->all());
        $this->validate(request(),[
            'title' => 'required|string',
           	'intro' => 'required',
            'content' => 'required'
            ]);
        return redirect('/pages');
    }

    public function delete(Page $page){
        return view('pages/delete', compact('page'));
    }

    public function destroy(Page $page){
        $page->delete();
        return redirect('pages/');
    }
}
