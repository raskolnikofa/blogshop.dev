<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;;

class OrdersController extends Controller
{
        public function show(){

        $data['orders'] = Order::all();
        return view('orders/show', $data);

    }

    public function show_one(Order $order){

        return view('orders/show_one', compact('order'));
    }

    public function create(){
        return view('orders/create');
    }

    public function store(){
        $this->validate(request(),[
            'customer_name' => 'required|string',
            'email' => 'required',
            'phone' => 'required',
            'feedback' => 'required'
        ]);

        Order::create(request()->all());
        return redirect('/orders');

    }

    public function delete(Order $order){
        return view('orders/delete', compact('order'));
    }

    public function destroy(Order $order){
        $order->delete();
        return redirect('orders/');
    }
}
