<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller
{

    public function show(){

        $data['products'] = Product::all();
        return view('products/show', $data);

    }

    public function show_one(Product $product){

        return view('products/show_one', compact('product'));
    }

    public function create(){
        return view('products/create');
    }

    public function store(){

        $this->validate(request(),[
            'title' => 'required|string',
            'alias' => 'required',
            'price' => 'required',
            'description' => 'required'
        ]);

        Product::create(request()->all());
        return redirect('/products');

    }

    public function edit(Product $product){

        return view('products/edit', compact('product'));

    }

    public function update(Product $product){
        $product->update(request()->all());
        $this->validate(request(),[
            'title' => 'required|string',
            'price' => 'required',
            'description' => 'required'
            ]);
        return redirect('/products');
    }

    public function delete(Product $product){
        return view('products/delete', compact('product'));
    }

    public function destroy(Product $product){
        $product->delete();
        return redirect('/products');
    }
}
